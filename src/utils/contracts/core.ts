import { Contract } from "ethers";
import TreasuryArtifact from "@/artifacts/contracts/Treasury.sol/Treasury.json";

export async function getTreasury(factory: any, provider: any) {
  const treasuryAddress = factory.treasury();

  return new Contract(
    treasuryAddress,
    TreasuryArtifact.abi,
    provider.getSigner()
  );
}
