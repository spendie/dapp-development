import { ethers, Contract } from "ethers";

import IUniswapV2Router02Artifact from "@uniswap/v2-periphery/build/IUniswapV2Router02.json";

export default async () => {
  const provider = ethers.getDefaultProvider("http://localhost:8545");
  const uniswapV2Router02 = new Contract(
    "0xa5E0829CaCEd8fFDD4De3c43696c57F7D7A678ff",
    IUniswapV2Router02Artifact.abi,
    provider
  );

  return await uniswapV2Router02.WETH();
};
